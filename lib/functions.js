function formatBitLen (str) { // to pad a byte inorder to make it 8 bits long
  var length = str.length
  var diff = 8 - length
  var str1 = str
  for (var i = 0;i < diff;i++) {
    str1 = '0' + str1
  }
  return str1
}

function parseDateData (str) {
  var date = {
    'year': '20' + parseInt(str.substr(0, 2), 16),
    'month': parseInt(str.substr(2, 2), 16),
    'day': parseInt(str.substr(4, 2), 16),
    'hour': parseInt(str.substr(6, 2), 16),
    'minute': parseInt(str.substr(8, 2), 16),
    'second': parseInt(str.substr(10, 2), 16),
  }
  return date
}

function parseGpsData (str) {
  var parsedVal = parseFloat(parseInt(str, 16))
  var temp = parsedVal / 30000.0
  var deg = parseInt(temp / 60)
  var minute = temp - deg * 60
  var minuteFix = parseFloat(minute.toFixed(4))
  var decimalCoord = deg + minuteFix / 60
  return decimalCoord.toFixed(4)
}

function parseCourseData (str) {
  var gpsInfo,gpsPos,gpsLong,gpsLat,gpsCourse
  var byte1 = str.substr(0, 2)
  var byte2 = str.substr(2, 2)
  var bit1 = parseInt(byte1, 16).toString(2)
  var bit2 = parseInt(byte2, 16).toString(2)
  var FormatterdBit1 = formatBitLen(bit1)
  var FormatterdBit2 = formatBitLen(bit2)
  var gpsInfoBit = FormatterdBit1.substr(5, 1)
  var gpsPosBit = FormatterdBit1.substr(4, 1)
  var gpsLongBit = FormatterdBit1.substr(3, 1)
  var gpsLatBit = FormatterdBit1.substr(2, 1)
  var gpsCourseBit = FormatterdBit1.substr(1, 1) + FormatterdBit1.substr(0, 1) + FormatterdBit2
  if (gpsInfoBit === '0') {
    gpsInfo = 'realTimeGps'
  } else {
    gpsInfo = 'differentialGps'
  }
  if (gpsPosBit === '0') {
    gpsPos = false
  } else {
    gpsPos = true
  }
  if (gpsLongBit === '0') {
    gpsLong = 'east'
  } else {
    gpsLong = 'west'
  }
  if (gpsLatBit === '0') {
    gpsLat = 'south'
  } else {
    gpsLat = 'north'
  }

  gpsCourse = parseInt(gpsCourseBit, 2).toString(10)
  var data = {
    'gpsInfo': gpsInfo,
    'gpsPos': gpsPos,
    'gpsLong': gpsLong,
    'gpsLat': gpsLat,
    'gpsCourse': gpsCourse
  }
  return data
}

function parseTerminalData (str) {
  var terminfoBit = parseInt(str, 16).toString(2)
  var terminfoFormatted = formatBitLen(terminfoBit)
  var data = {
  }
  if (terminfoFormatted.substr(0, 1) === '1') {
    data['activated'] = true
  } else {
    data['activated'] = false
  }
  if (terminfoFormatted.substr(1, 1) === '1') {
    data['ACC'] = 'high'
  } else {
    data['ACC'] = 'low'
  }
  if (terminfoFormatted.substr(2, 1) === '1') {
    data['charge'] = true
  } else {
    data['charge'] = false
  }
  if (terminfoFormatted.substr(3, 3) === '000') {
    data['type'] = 'normal'
  }
  else if (terminfoFormatted.substr(3, 3) === '001') {
    data['type'] = 'shock'
  }
  else if (terminfoFormatted.substr(3, 3) === '010') {
    data['type'] = 'powerCut'
  }
  else if (terminfoFormatted.substr(3, 3) === '011') {
    data['type'] = 'lowBattery'
  }
  else if (terminfoFormatted.substr(3, 3) === '100') {
    data['type'] = 'sos'
  } else {
    console.log('not defined in parseterminaldata in functions')
  }
  if (terminfoFormatted.substr(6, 1) === '1') {
    data['gpstracking'] = true
  } else {
    data['gpstracking'] = false
  }
  if (terminfoFormatted.substr(2, 1) === '1') {
    data['oilElec'] = false
  } else {
    data['oilElec'] = true
  }
  return data
}

function parseAlarmData (str) {
  var voltLevel,gsmSignal,lang,voltmsg,gsmMsg,alarmType,alarmMsg,alarmLangCode,alarmLang,termInfoContent
  voltLevel = parseInt(str.substr(2, 2), 16).toString(10)
  gsmSignal = parseInt(str.substr(4, 2), 16).toString(10)
  alarmType = parseInt(str.substr(6, 2), 16).toString(10)
  alarmLangCode = parseInt(str.substr(8, 2), 16).toString(10)
  termInfoContent = parseTerminalData(str.substr(0, 2))
  switch (voltLevel) {
    case '0':
      voltmsg = 'noPower'
      break
    case '1':
      voltmsg = 'extremelyLow'
      break
    case '2':
      voltmsg = 'veryLow'
      break
    case '3':
      voltmsg = 'low'
      break
    case '4':
      voltmsg = 'medium'
      break
    case '5':
      voltmsg = 'high'
      break
    case '6':
      voltmsg = 'veryHigh'
      break
  }
  switch (gsmSignal) {
    case '0':
      gsmMsg = 'noSignal'
      break
    case '1':
      gsmMsg = 'extremelyWeak'
      break
    case '2':
      gsmMsg = 'veryWeak'
      break
    case '3':
      gsmMsg = 'good'
      break
    case '4':
      gsmMsg = 'strong'
      break
  }
  switch (alarmType) {
    case '0':
      alarmMsg = 'normal'
      break
    case '1':
      alarmMsg = 'sos'
      break
    case '2':
      alarmMsg = 'powerCut'
      break
    case '3':
      alarmMsg = 'shock'
      break
    case '4':
      alarmMsg = 'fenceIn'
      break
    case '5':
      alarmMsg = 'fenceOut'
      break
  }
  if (alarmLangCode == 1) {
    alarmLang = 'chinese'
  } else {
    alarmLang = 'english'
  }
  return {
    'termInfoContent': termInfoContent,
    'voltLevel': voltmsg,
    'gsmSignal': gsmMsg,
    'alarmLang': {
      'alarmType': alarmMsg,
      'language': alarmLang
    }
  }
}

function parsePingData (str) {
  var data = {
    'date': parseDateData(str.substr(0, 12)),
    'GpsSatNum': parseInt(str.substr(13, 1), 16),
    'latitude': parseGpsData(str.substr(14, 8)),
    'longitude': parseGpsData(str.substr(22, 8)),
    'speed': parseInt(str.substr(30, 2), 16),
    'course': parseCourseData(str.substr(32, 4)),
    'Mcc': parseInt(str.substr(36, 4), 16),
    'Mnc': parseInt(str.substr(40, 2), 16),
    'Lac': parseInt(str.substr(42, 4), 16),
    'CellTowerId': parseInt(str.substr(46, 6), 16)
  }
  return data
}

function getParsedAlarmData (str) {
  var data = {
    'date': parseDateData(str.substr(0, 12)),
    'GpsSatNum': parseInt(str.substr(13, 1), 16),
    'latitude': parseGpsData(str.substr(14, 8)),
    'longitude': parseGpsData(str.substr(22, 8)),
    'speed': parseInt(str.substr(30, 2), 16),
    'course': parseCourseData(str.substr(32, 4)),
    'LbsDataLength': str.substr(36, 2),
    'Mcc': parseInt(str.substr(38, 4), 16),
    'Mnc': parseInt(str.substr(42, 2), 16),
    'Lac': parseInt(str.substr(44, 4), 16),
    'CellTowerId': parseInt(str.substr(48, 6), 16),
    'alarmData': parseAlarmData(str.substr(54, 10))
  }
  return data
}

function getParsedStatusData (str) {
  var data = parseAlarmData(str)
  return data
}

exports.parsePingData = parsePingData
exports.getParsedAlarmData = getParsedAlarmData
exports.getParsedStatusData = getParsedStatusData
