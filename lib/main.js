f = require('./functions.js')

function parse (data) {
  data = data.toString()
  data = data.replace(/\s/g, '') // remove whitespaces    
  var protocolNum = data.substr(6, 2)
  var parts = {
    'start': data.substr(0, 4),
    'packetLength': data.substr(4, 2),
    'protocolNumber': data.substr(6, 2), // mandatory
    'finish': data.substr(data.length - 4, 4)
  }
  // parts=start,packetLength,protocolNumber,infoContent,infoNum,errorCheck,finish
  switch (parts.protocolNumber) {
    case '01':
      parts.action = 'login_request'
      parts.infoContent = data.substr(8, 16)
      parts.infoSrNum = data.substr(24, 4)
      parts.errorCheck = data.substr(28, 4)
      break
    case '12':
      parts.action = 'ping'
      parts.infoContent = f.parsePingData(data.substr(8, 52))
      parts.infoSrNum = data.substr(60, 4)
      parts.errorCheck = data.substr(64, 4)
      break
    case '16':
      parts.action = 'alarm'
      parts.infoContent = f.getParsedAlarmData(data.substr(8, 64))
      parts.infoSrNum = data.substr(72, 4)
      parts.errorCheck = data.substr(76, 4)
      break
    case '13':
      parts.action = 'status'
      parts.infoContent = f.getParsedStatusData(data.substr(8, 10))
      parts.infoSrNum = data.substr(18, 4)
      parts.errorCheck = data.substr(22, 4)
      break
  }
  return parts
}

module.exports.parse = parse
